# HCloud-User-SDK

### 介绍
HCloud是一个云际计算平台。
HCloud致力于结合不同云平台在安全性、功能性以及性价比上的差异，为用户提供满足不同业务场景需求的高质量云服务。

### 软件架构
HCloud平台采用函数即服务（FaaS)模式，允许开发者上传 **Service** 并且允许普通用户通过发起一个 **Task** 进行 **Service** 的调用。

### 安装教程

#### 安装用户SDK

```
    git clone https://gitee.com/huazhichao/HCloud.git
```

#### 使用说明

#### 1. 编译用户SDK

```
    cd HCloud
    make libs
```

#### 2. 编译测试Demo

```
    make test
```

#### 3. 运行测试Demo

```
    ./test
```
