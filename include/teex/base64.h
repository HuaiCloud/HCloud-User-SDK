#ifndef _BASE64_H_
#define _BASE64_H_


#define B64_ENCODE_LEN(x) (((x)+2)/3*4)
#define B64_DECODE_LEN(x) (((x)+3)/4*3)

#ifdef __cplusplus
extern "C" {
#endif

int base64_encode(const void *data, int size, char *code);
int base64_decode(const char *code, void *data);

#ifdef __cplusplus
}
#endif


#endif
