#ifndef __TEEX_CONST_H__
#define __TEEX_CONST_H__



#define PUBLIC_KEY_LEN		(40)
#define PRIVATE_KEY_LEN		(64)
#define ID_LEN				(32)
#define CONTRACT_ADDR_LEN	(40)
#define PRICE_LEN			(64)
#define HASH_LEN			(32)


#define DEFAULT_CHAIN_ADDR			"120.132.14.182"
#define DEFAULT_CHAIN_PORT			9545
#define DEFAULT_SERVICE_CONTRACT	"aeFC4E7143e8896D6aA67f5352372c7b0D233b9b"
#define DEFAULT_TOKEN_CONTRACT		"B127af3CDE8ba755F42553c383Fd368BC4f8C25a"


#endif
