#ifndef __TEEX_FILE_H__
#define __TEEX_FILE_H__

#include "teex/error.h"
#include "cjson/cJSON.h"

enum teex_file_type_t{
	TEEX_DIR = 0,
	TEEX_FILE,
};


typedef struct teex_file_t {
	teex_file_type_t type;					/* type of the file, TEEX_DIR or TEEX_FILE */
	char *name;								/* file name */
	
	union {
		struct {
			int   size;						/* size of a regular file */
			unsigned char *text;			/* content of the file */
		} file;

		struct {					
			int	   fileNumber;				/* number of files in a directory */
			struct teex_file_t **files;		/* each file in the directory */
		} dir;
	} body;									/* file body */

} teexFile;



/* loadFile
 * Purpose: Load a file/directory from the disk.
 *
 * Parameters:
 *		fileDir - [IN] the directory of the service code.
 *		file - [OUT] the loaded service code structure.
 *
 * Return value:
 *		teex_status_t - TEEX_SUCCESS or failure as defined in error.h
 */
teex_status_t loadFile(char *fileDir, teexFile *file);

/* calFileHash
 * Purpose: calculate the SHA256 hash of the file.
 *
 * Parameters:
 *		file - [IN] the input teexFile structure.
 *		fileHash - [out] the SHA256 hash hexstring of the file.
 *
 * Return value:
 *		teex_status_t - TEEX_SUCCESS or failure as defined in error.h
 */
teex_status_t calFileHash(teexFile *file, char **fileHash);

/* fileToJson
 * Purpose: transfer the teexFile to a JSON object
 * 
 * Parameters:
 *		file - [in] the input teexFile structure.
 *		fileJson - [out] the output JSON object.
 *
 * Return value:
 *		teex_status_t - TEEX_SUCCESS or failure as defined in error.h
 */
 teex_status_t fileToJson(teexFile *file, cJSON *fileJson);

 /* jsonToFile
  * Purpose: transfer the JSON object to teexFile
  * 
  * Parameters:
  *		fileJson - [in] the input JSON object.
  *		file - [out] the output teexFile structure.
  *	
  * Return value:
  *		teex_status_t - TEEX_SUCCESS or failure as defined in error.h
  */
  teex_status_t jsonToFile(cJSON *fileJson, teexFile *file);

#endif
