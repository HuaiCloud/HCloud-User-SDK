#ifndef __TEEX_INVOKE_H__
#define __TEEX_INVOKE_H__

#include "teex/error.h"

typedef struct teex_task_t {
	char * taskID;			/* ID for the task, generated by create_task_onchain */
	char * serviceID;		/* ID for the invoked service */
	char * input;			/* Must be a printable string */
	int    dataNumber;		/* Number of the data used by the task */
	char **dataList;		/* Array of the data IDs */

	char *publicKey;		/* Public key of the user */
	char *price;			/* Price paid for the task, a printable string */
} teexTask;


/* createTaskOnChain
 * Purpose: Create a task on the blockchain and get the taskID.
 *
 * Parameters:
 *		chainAddr - [IN] address of the chain node.
 *		chainPort - [IN] port of the chain node.
 *		privateKey - [IN] private key of the user wallet.
 *		publicKey - [IN] public key of the user wallet.
 *		serviceContractAddr - [IN] the smart contract address of the TEEX service.
 *		tokenContractAddr - [IN] the smart contract address of the TEEX token.
 *		task_info - [IN] the task information w/o the task ID.
 *		taskID - [OUT] the task ID generated by the blockchain.
 *		timeout - [IN] time for confirming the created task onchain, in seconds
 *
 * Return value:
 *		teex_status_t - TEEX_SUCCESS or failure as defined in error.h
 */
teex_status_t createTaskOnChain(char *chainAddr, int chainPort, char *privateKey, 
							char *publicKey, 
							char *serviceContractAddr, char *tokenContractAddr,
							teexTask *taskInfo, char **taskID,
							unsigned int timeout);


/* runTask
 *
 * Parameters:
 *		dispatcherURL - [IN] url of a TEEX dispatcher.
 *		dispatcherPort - [IN] port of a TEEX dispatcher.
 *		taskInfo - [IN] the task information.
 *		returnMsg - [OUT] the returned string of the task.
 *		msgLen - [OUT] the size of the returned string.
 *
 * Return value:
 *		teex_status_t - TEEX_SUCCESS or failure as defined in error.h
 */
teex_status_t runTask(char *dispatcherURL, int dispatcherPort,
							teexTask *taskInfo, char **returnMsg,
							int *msgLen);

#endif
