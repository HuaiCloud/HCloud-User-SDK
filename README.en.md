# HCloud

#### Description
HCloud是一个分布式可信云际计算平台。
HCloud致力于跨云环境下的可信数据融合。目前各云平台上储存了来自不同个人、企业用户的大量数据。由于安全、合规等不同问题，这些数据难以进行融合使用，极大的限制了数据所能够产生的价值。本项目利用密码学、TEE等技术，提供了一个跨云的可信计算环境，来自多个云平台的数据能够在可信环境中进行处理，从而获得数据融合之后的结果。

#### Software Architecture
Software architecture description

#### Installation

1. xxxx
2. xxxx
3. xxxx

#### Instructions

1. xxxx
2. xxxx
3. xxxx

#### Contribution

1. Fork the repository
2. Create Feat_xxx branch
3. Commit your code
4. Create Pull Request


#### Gitee Feature

1. You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2. Gitee blog [blog.gitee.com](https://blog.gitee.com)
3. Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4. The most valuable open source project [GVP](https://gitee.com/gvp)
5. The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6. The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)