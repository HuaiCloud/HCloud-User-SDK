#ifndef PUBLICCHAIN_H
#define PUBLICCHAIN_H

#include<stdint.h>
#include"ethrpc.h"
#include"ethrpc_type.h"



#define MAX_DESC_LENGTH  128

using namespace std;


typedef struct _dataBlock{

    char*           dataID;
    Address         owner;
    Hash            data_hash;
    uint256         size;
    uint256         price;
    char*           url;            // end with '\0'
    uint32_t        score;

} Datablock;
                                                            
typedef struct _service{      

    char*            servideID;
    Address          sp_addr;        //Holds a 20 byte value (size of an Ethereum address)
    Address          service_pk; 
    char*            codeHash;
    uint256          price;
    uint256          worker_feerate;
    char*            desc;           //the description of the service(can be a url)

    uint32_t         data_num;
    char**           bound_data_list;

} Service;


typedef struct _task{

    char*           taskID;
    Address         pk_user; 
    char*           servideID;
    uint32_t        timeout;
    uint256         payment;        //price for teex
    Hash            input_hash;
    uint32_t        status;

    uint32_t         data_num;
    char**           bound_data_list;

} Task;

typedef struct _receipt{

    char*           taskID;
    uint32_t        workerNum;
    Address*        workerList;
    uint32_t*       countList;
    unsigned char*  sig;

    uint32_t        dp_num;
    uint32_t*       dp_status_list;

} Receipt;


typedef struct _tasklist{
    uint32_t        size;
    Task*           tasks;        
} TaskList;


typedef struct _servicelist{
    uint32_t        size;
    Service*        services;        

} ServiceList;


class PublicChain{

public:

        PublicChain(const char* url, const char* Bounty_Contract_Address, const char* TEEX_Contract_Address, const char* sk, const char* pk);

        ~PublicChain();

        uint32_t    getServiceLength();

        uint32_t    getServiceList(ServiceList& res);

        uint32_t    getServiceByID(char* id, Service& res);

        uint32_t    newService(Service* service);

        uint32_t    getTaskListByUser(Address addr, TaskList& res);

        uint32_t    getTaskByID(char* id, Task& res);

        uint32_t    newTask(Task* task);

        uint32_t    verifyTask(Receipt* receipt);

        uint32_t    abortTask(Receipt* receipt);

        uint32_t    recallTask(char* taskID);

        uint32_t    checkService(string txnhash, Service* service);

        char*       getBalance(const char* address);

        uint32_t    newDataBlock(Datablock* dataBlock);

        uint32_t    getDataBlockByID(char* id, Datablock& dataBlock);
        

        // variables
        uint32_t status;  

        ETHRPC*  ethrpc;

        char* pk = new char[43];

        char* TEEX_Contract_Address = new char[43];

        char* Bounty_Contract_Address = new char[43];
        


private:
        
        
        uint32_t   getRandom();

        uint32_t    approveToken(Address from, uint256 amount);



};





#endif /* PUBLICCHAIN_H */