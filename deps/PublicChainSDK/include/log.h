#ifndef LOG_H
#define LOG_H

#include <stdio.h>
#include <stdlib.h>


// #define __TEEX_DEBUG__  
#ifdef  __TEEX_DEBUG__  
#define DEBUG(format,...) printf("\033[36m[TEEX-DEBUG]File: " __FILE__ ", Line: %04d: " format "\033[0m\n", __LINE__ , ##__VA_ARGS__)  
#else  
#define DEBUG(format,...)  
#endif  



#endif