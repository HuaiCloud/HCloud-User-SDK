#ifndef _TEEX_RA_H_
#define _TEEX_RA_H_


#include <stdint.h>

typedef uint32_t teex_mutual_ra_context_t;

typedef enum _teex_mutual_ra_status_t {
	TEEX_MUTUAL_RA_SUCCESS = 0x0000,
	TEEX_MUTUAL_RA_ERROR_UNEXPECTED = 0x0001,
	TEEX_MUTUAL_RA_INVALID_PARAMETER = 0x0002,
	TEEX_MUTUAL_RA_INVALID_STATE = 0x0003,
	TEEX_MUTUAL_RA_EID_NOT_SETTED = 0x0004,
	TEEX_MUTUAL_RA_INVALID_MESSAGE = 0x1001,
	TEEX_MUTUAL_RA_MAC_VERIFY_ERROR = 0x1002,
	TEEX_MUTUAL_RA_REPORT_VERIFY_ERROR = 0x1003,
	TEEX_MUTUAL_RA_USER_VERIFY_ERROR = 0x1004
} teex_mutual_ra_status_t;

typedef enum _mutual_attestation_status_t {
	Mutual_Attestation_NotTrusted = 0,
	Mutual_Attestation_Trusted = 1
} mutual_attestation_status_t;

typedef struct _teex_mutual_ra_msg1_t {
	uint32_t extended_epid_group_id;    // 4 bytes
	sgx_epid_group_id_t gid;    // 4 bytes
	sgx_ec256_public_t g_a;    // 64 bytes
} teex_mutual_ra_msg1_t;

typedef struct _teex_mutual_ra_msg2_t {
	sgx_mac_t mac;    // 16 bytes
	sgx_spid_t spid;    // 16 bytes
	uint32_t quote_type;    // 4 bytes
	uint32_t sig_rl_size;    // 4 bytes
	uint8_t sig_rl[];
} teex_mutual_ra_msg2_t;

typedef struct _teex_mutual_ra_msg3_t {
	sgx_mac_t mac;    // 16 bytes
	sgx_quote_t quote;
} teex_mutual_ra_msg3_t;

typedef struct _teex_mutual_ra_msg4_t {
	sgx_mac_t mac;    // 16 bytes
	mutual_attestation_status_t status;    // 4 bytes
	// sgx_platform_info_t platformInfoBlob;   // 101 bytes
} teex_mutual_ra_msg4_t;
// TODO teex_mutual_ra_msg4_t


#endif

