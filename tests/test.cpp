#include <stdio.h>
#include <string.h>
#include <stdlib.h>


#include "teex/task.h"
#include "teex/const.h"
#include "teex/file.h"
#include "teex/service.h"

char* create_service(char *serviceDir, char *serviceEntry)
{
	teexFile *file = (teexFile*)malloc(sizeof(teexFile));
	teex_status_t retval;

	char *pubKey = "8F2d0Fa9995Eb0B041B48fBCbC0f92bFe6CFb8B5";
	char *prvKey = "708a16c5642ec951e4c11b819d7c5f30b00fdacbf2c8109f6d707f0796544599";
	
	/* Step-0: load the service code */
	if ((retval = loadFile(serviceDir, file)) != TEEX_SUCCESS) {
		printf("load file %s error %x\n", serviceDir, retval);
		return NULL;
	}
	

	/* Step-1: init the service info */
	teexService *serviceInfo = (teexService*)malloc(sizeof(teexService));
	serviceInfo->price = "002";
	serviceInfo->workerFeerate = "001";
	serviceInfo->desc = "HelloWorld Demo";
	serviceInfo->providerAddr = pubKey;
	serviceInfo->publicKey = pubKey;
	serviceInfo->privateKey = prvKey;

	serviceInfo->code = file;
	serviceInfo->codeEntry = serviceEntry;

	/* calculate the hash of the service code */
	if ((retval = calFileHash(file, &serviceInfo->codeHash)) != TEEX_SUCCESS) {
		printf("cal code hash failed %x\n", retval);
		return NULL;
	}

	serviceInfo->runtime = "python3.7";
	serviceInfo->dataNumber = 0;
	serviceInfo->dataList = NULL;


	/* Step-2: create the service on the blockchain */
	if ((retval = createServiceOnChain(DEFAULT_CHAIN_ADDR, DEFAULT_CHAIN_PORT,
											prvKey, pubKey,
											DEFAULT_SERVICE_CONTRACT, DEFAULT_TOKEN_CONTRACT,
											serviceInfo, &serviceInfo->serviceID,
											10)) != TEEX_SUCCESS) {
		printf("create service onchain Error %x\n", retval);
		return NULL;
	}

	/* Step-3: deploy the service on the HCloud and get the service ID 
	 * user should provide the ip and port of the service manager
	 */
	char *errMsg = NULL;	
	if ((retval = deployService("127.0.0.1", 12000, serviceInfo, &errMsg)) != TEEX_SUCCESS) {
		printf("deploy Service Error %x\n", retval);
		return NULL;
	}


	/* return the service ID */
	return strdup(serviceInfo->serviceID);
}


int create_task(char *serviceID)
{
	teexTask Task;



	/* Step-0: Init the Task Information */
	Task.serviceID = strdup(serviceID);
	Task.input = "thw=cn; cna=OLFOFZBoYVMCAcp4KFKCxC+z; t=525543e96ee924b2c875dddeeadefffa; hng=CN%7Czh-CN%7CCNY%7C156; tg=0; x=e%3D1%26p%3D*%26s%3D0%26c%3D0%26f%3D0%26g%3D0%26t%3D0; _fbp=fb.1.1563181673122.354682555; cookie2=1ab9503d242b5105d11a8d485761803e; _tb_token_=5e33a581349e7; v=0; _m_h5_tk=0857521e057d61757c0e7fbe44bd8c41_1563449138217; _m_h5_tk_enc=5301a04fa21ce1fe3a36e004bf64f21f; l=cB_qUzzrqxUeQXGBKOCZSuI8Lu79jIRAguPRwCYMi_5CTs81VM_OkqhXMUJ6cfWd_jTB4wpNddy9-eteiHJXJfIpXUJ1.; isg=BG9vIli2btZ81Wr_03QtCEnj_oq5vMIynA69k4H8Gl7l0I_Siea8h32CUojLqJuu; unb=687744600; uc1=cookie16=VT5L2FSpNgq6fDudInPRgavC%2BQ%3D%3D&cookie21=UtASsssmeWzt&cookie15=V32FPkk%2Fw0dUvg%3D%3D&existShop=false&pas=0&cookie14=UoTaG7u41g%2BfgA%3D%3D&tag=8&lng=zh_CN; sg=701; _l_g_=Ug%3D%3D; skt=0de1b21d8376d7e1; cookie1=W8yen59pXqphr176w4WvBnATyFgXZpq0xDOZLfM3w0w%3D; csg=63e8c5da; uc3=vt3=F8dBy3zdlkhhgg66QQU%3D&id2=VWeWQ6lYS6VW&nk2=AHnXtAK1t91wrQ%3D%3D&lg2=URm48syIIVrSKA%3D%3D; existShop=MTU2MzQzOTQzNw%3D%3D; tracknick=cca7777777; lgc=cca7777777; _cc_=UIHiLt3xSw%3D%3D; dnk=cca7777777; _nk_=cca7777777; cookie17=VWeWQ6lYS6VW; mt=ci=-1_0&np=";

	/* data Number could be zero */
	Task.dataNumber = 0;
	Task.dataList = NULL;

	Task.publicKey = "8F2d0Fa9995Eb0B041B48fBCbC0f92bFe6CFb8B5";
	Task.price = "2";

	char *newTaskID = NULL;
	char *pubKey = "8F2d0Fa9995Eb0B041B48fBCbC0f92bFe6CFb8B5";
	char *prvKey = "708a16c5642ec951e4c11b819d7c5f30b00fdacbf2c8109f6d707f0796544599";
	char *buf = (char*)malloc(1024);
	int r = 0;

	memset(buf, 0, 1024);
	sprintf(buf, "http://%s:%d", DEFAULT_CHAIN_ADDR, DEFAULT_CHAIN_PORT);


	/* Step-1: create the task on the blockchain */
	if ((r = createTaskOnChain(DEFAULT_CHAIN_ADDR, DEFAULT_CHAIN_PORT, prvKey, pubKey, DEFAULT_SERVICE_CONTRACT, DEFAULT_TOKEN_CONTRACT, &Task, &newTaskID, 10)) != TEEX_SUCCESS) {
		printf("Create Task Error: %x\n", r);
		return -1;
	}

	Task.taskID = newTaskID;
	char *returnMsg = NULL;
	int msgLen = 0;

	/* Step-2: Invoke the task 
	 * User should provide the ip and port of the disptacher
	 */
	if ((r = runTask("127.0.0.1", 9080, &Task, &returnMsg, &msgLen)) != TEEX_SUCCESS) {
		printf("Run Task Error: %x\n", r);
		if (r == TEEX_ERROR_WORKER_EXECUTION) 
			printf("Error Msg is %s\n", returnMsg);
		return -1;
	}

	printf("Execution Result\n%s\n", returnMsg);

	return 0;
}


int main()
{
	/* the dir of the service code and the entry file of the service code */
	char *serviceID = create_service("sampleService/taoBaoSpider", 
								"sampleService/taoBaoSpider/taobao_spider.py");

	/* create and invoke the task */
	if (serviceID != NULL)
		create_task(serviceID);
}
